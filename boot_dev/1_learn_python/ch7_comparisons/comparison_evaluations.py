def compare_heights(elon_height, sara_height, jill_height, bob_height):
    is_bob_as_tall_as_elon = bob_height == elon_height
    is_sara_as_tall_as_elon = sara_height == elon_height
    is_jill_as_tall_as_sara = jill_height == elon_height
    return is_bob_as_tall_as_elon, is_sara_as_tall_as_elon, is_jill_as_tall_as_sara
