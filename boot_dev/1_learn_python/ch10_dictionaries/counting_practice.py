def count_enemies(enemy_names):
    count = {}
    for enemy in enemy_names:
        if enemy not in count:
            count[enemy] = 1
        else:
            count[enemy] += 1
    return count
