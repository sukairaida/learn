def double_string(string):
    doubled = []
    for char in string:
        doubled.append(char+char)
    return "".join(doubled)
