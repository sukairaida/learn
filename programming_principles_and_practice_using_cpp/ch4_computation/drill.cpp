
#include "../std_lib_facilities.h"

int constexpr m_to_cm = 100;
double constexpr in_to_cm = 2.54;
double constexpr ft_to_cm = 12 * in_to_cm;

bool check_valid_unit(string value, vector<string> units);
double get_meters_value(double value, string unit);

int main()
{
    double smallest, largest;
    double current;
    string unit;
    vector<string> units = {"m", "cm", "ft", "in"};
    int i = 0;
    double sum = 0;
    cout << "Please enter one value at a time followed by its unit: ";
    vector<double> values;
    while (cin >> current >> unit)
    {
        if (!check_valid_unit(unit, units))
        {
            cout << "Invalid unit!\n";
            continue;
        }

        double meters_value = get_meters_value(current, unit);

        if (i == 0 || meters_value > largest)
        {
            largest = meters_value;
            cout << largest << " the largest so far\n";
        }

        if (i == 0 || meters_value < smallest)
        {
            smallest = meters_value;
            cout << smallest << " the smallest so far\n";
        }
        sum += meters_value;
        values.push_back(meters_value);
        ++i;
    }

    cout << "All the introduced values AS METERS were:\n";
    sort(values);
    for (double value : values)
    {
        cout << value << "\n";
    }
    cout << "The smallest value was: " << smallest << " meters\n";
    cout << "The largest value was: " << largest << " meters\n";
    cout << "The sum of all the values in meters was: " << sum << "\n";

    return 0;
}

bool check_valid_unit(string value, vector<string> units)
{
    for (string unit : units)
    {
        if (value == unit)
        {
            return true;
        }
    }
    return false;
}

double get_meters_value(double value, string unit)
{
    if (unit == "cm")
    {
        return value / 100;
    }
    else if (unit == "in")
    {
        return (value * in_to_cm) / 100;
    }
    else if (unit == "ft")
    {
        return (value * ft_to_cm) / 100;
    }
    else
    {
        return value;
    }
}
