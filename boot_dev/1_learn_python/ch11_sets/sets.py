def remove_duplicates(spells):
    unique = set()
    for spell in spells:
        unique.add(spell)
    asList = []
    for spell in unique:
        asList.append(spell)
    return asList

