
#include "../std_lib_facilities.h"

int main()
{
    int minimum = 0;
    int maximum = 100;
    int guesses = 0;
    cout << "Think about a number and I will guess it.\n";
    while (guesses < 8)
    {
        int current = (maximum + minimum) / 2;
        string response;
        cout << "Is the number you are thinking about " << current << " ? type 'yes', 'higher' or 'lower': ";
        cin >> response;
        if (response == "yes")
        {
            "Thanks for playing!";
            break;
        }
        else if (response == "lower")
        {
            maximum = current;
        }
        else if (response == "higher")
        {
            minimum = current;
        }
        else
        {
            "Invalid response, try again!";
        }
        guesses++;
    }

    if (guesses == 8)
    {
        cout << "You are cheating!\n";
    }

    return 0;
}