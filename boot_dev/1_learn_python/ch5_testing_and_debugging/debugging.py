def take_magic_damage(health, resist, amp, spell_power):
    amp_damage = spell_power * amp
    real_damage = amp_damage - resist
    return health - real_damage
