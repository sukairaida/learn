#include "../std_lib_facilities.h"

bool check_duplicate(string name, vector<string> names);
void print_scores(vector<string> names, vector<int> scores);

int main(){
    vector<string> names;
    vector<int> scores;
    string current_name;
    int current_score;
    cout << "Enter a name followed by its score, end input with name NoName and score 0: ";
    for (; cin >>current_name >>current_score;){
        if (current_name == "NoName" && current_score == 0){
            break;
        }
        if (check_duplicate(current_name, names)){
            simple_error("Duplicate name found, terminating program.");
        }
        names.push_back(current_name);
        scores.push_back(current_score);

    }

    print_scores(names,scores);

    return 0;
}

bool check_duplicate(string name, vector<string> names){
    for (string n : names){
        if (n == name){
            return true;
        }
    }
    return false;
}

void print_scores(vector<string> names, vector<int> scores){
    for (int i = 0; i < names.size(); ++i){
        cout << names[i] << ": " << scores[i] << "\n";
    }
}
