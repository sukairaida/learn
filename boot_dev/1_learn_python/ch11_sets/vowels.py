def count_vowels(text):
    count = 0
    vowels = ['a','e','i','o','u']
    unique = set()
    for char in text:
        if (char.lower() in vowels):
            unique.add(char)
            count += 1
    return count, unique
