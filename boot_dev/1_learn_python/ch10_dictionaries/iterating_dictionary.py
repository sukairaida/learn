def get_most_common_enemy(enemies_dict):
    max = float("-inf")
    most_common = None
    for enemy in enemies_dict:
        if enemies_dict[enemy] > max:
            most_common = enemy
            max = enemies_dict[enemy]
    return most_common