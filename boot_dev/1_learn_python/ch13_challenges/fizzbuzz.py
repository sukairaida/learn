def fizzbuzz(start, end):
    for i in range(start, end):
        result = ""
        if (i % 3 == 0):
            result += 'fizz'
        if (i % 5 == 0):
            result += 'buzz'
        if (result == ""):
            print(i)
        else:
            print(result)


# Don't Touch Below This Line


def main():
    test(1, 100)
    test(5, 30)
    test(1, 15)


def test(start, end):
    print("Starting test")
    fizzbuzz(start, end)
    print("======================")


main()
