def filter_messages(messages):
    filtered_messages = []
    filtered_dangs = []
    for message in messages:
        bad_count = 0
        good_words = []
        for word in message.split():
            if word == "dang":
                bad_count += 1
            else:
                good_words.append(word)
        filtered_messages.append(" ".join(good_words))
        filtered_dangs.append(bad_count)
    return filtered_messages, filtered_dangs

