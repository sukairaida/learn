def merge(dict1, dict2):
    for score in dict2:
        dict1[score] = dict2[score]
    return dict1

def total_score(score_dict):
    total = 0
    for score in score_dict:
        total += score_dict[score]
    return total
