#include "../std_lib_facilities.h"

int main()
{
    cout << "Enter the name of the person you want to write to: ";
    string first_name;
    cin >> first_name;
    cout << "Dear " << first_name << ",\n";
    cout << "How are you? I am fine. I miss you.\n";
    cout << "Enter the name of another friend: ";
    string friend_name;
    cin >> friend_name;
    cout << "Have you seen " << friend_name << " lately?\n";
    char friend_sex = 0;
    cout << "Enter a 'm' if the friend is a male, 'f' if it is a female: ";
    cin >> friend_sex;
    string first_part = "if you see " + friend_name + " ask ";
    string second_part = " to call me.\n";
    if (friend_sex == 'm')
    {
        cout << first_part << "him" << second_part;
    }
    else
    {
        cout << first_part << "her" << second_part;
    }
    cout << "Enter the recipient's age: ";
    int age;
    cin >> age;
    cout << "I hear you just had a birthday and you are" << age << "years old.\n";
    if (age <= 0 || age >= 110)
    {
        simple_error("you're kidding");
    }
    if (age < 12)
    {
        cout << "Next year you will be " << age + 1 << " years old.";
    }
    if (age == 17)
    {
        cout << "Next year you will be able to vote.";
    }
    if (age > 70)
    {
        cout << "I hope you are enjoying your retirement.";
    }
    cout << "\nYours sincerely...\n";

    return 0;
}