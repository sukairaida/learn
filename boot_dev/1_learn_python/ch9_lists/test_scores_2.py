def get_test_score(answer_sheet, student_answers):
    correct = 0
    no_name_answers = student_answers[1:]
    for i in range(0, len(answer_sheet)):
        if (no_name_answers[i] == answer_sheet[i]):
            correct +=1
    score = correct / len(answer_sheet) * 100
    return student_answers[0], score
