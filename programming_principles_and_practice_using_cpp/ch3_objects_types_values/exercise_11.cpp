#include "../std_lib_facilities.h"

int main()
{
    int pennies, nickels, dimes, quarters, half_dollars, one_dollars;

    cout << "Number of pennies: ";
    cin >> pennies;
    cout << "Number of nickels: ";
    cin >> nickels;
    cout << "Number of dimes: ";
    cin >> dimes;
    cout << "Number of quarters: ";
    cin >> quarters;
    cout << "Number of half dollars: ";
    cin >> half_dollars;
    cout << "Number of one dollars: ";
    cin >> one_dollars;
    double total = one_dollars;
    total += pennies / 100.0;
    total += (nickels * 5) / 100.0;
    total += (dimes * 10) / 100.0;
    total += (quarters * 25) / 100.0;
    total += (half_dollars * 50) / 100.0;
    cout << "You have $" << total << " dollars.\n";

    return 0;
}