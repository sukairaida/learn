def calculate_experience_points(level):
    total = 0
    for i in range(1, level):
        total += i*5
    return total