#include "../std_lib_facilities.h"

bool verify_prime(int number, vector<int> primes);
void print_primes(vector<int> primes);

int main(){

    vector<int> primes = {2};

    for (int i = 3; i <= 100; ++i){
        if (verify_prime(i, primes)){
            primes.push_back(i);
        }
    }

    print_primes(primes);

    return 0;
}

bool verify_prime(int number, vector<int> primes){
    for (int prime : primes){
        if (number % prime == 0){
            return false;
        }
    }
    return true;
}

void print_primes(vector<int> primes){
    for (int prime: primes){
        cout << prime << "\n";
    }
}
