def get_test_score(answer_sheet, student_answers):
    correct = 0
    for i in range(0, len(answer_sheet)):
        if (answer_sheet[i] == student_answers[i]):
            correct += 1
    score = (correct / len(answer_sheet)) * 100
    return score
